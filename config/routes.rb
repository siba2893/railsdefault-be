Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :v1, defaults: { format: 'json' } do
    scope :home do
      get '/', to: 'home#index'
      get 'needs_auth', to: 'home#needs_auth'
    end
  end

end
