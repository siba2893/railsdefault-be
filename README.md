# README

Backend API boilerplate for LYNXLABS projects.

Contains:
- JWT token based authentication
- ActiveAdmin Backoffice
- Rspec test suite

## Ruby version: 

2.6.5

## RVM Gemset
 
lynxlabs-be

## Configuration

Once cloned, cd to the repo directory. If RVM is installed, the gemset should be created automatically.

Run `bundle`

Followed by `rails db:setup db:seed`

Run the server `rails s` 

## Testing

This project uses Rspec, please write tests accordingly. 

To run the suite execute `rspec`