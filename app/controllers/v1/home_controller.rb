module V1
  class HomeController < ApiController
    before_action :authenticate_user!, except: :index

    def index
      render json: { message: 'Hello Lynx!' }
    end

    def needs_auth
      render json: { message: 'Hello Friend!' }
    end

  end
end